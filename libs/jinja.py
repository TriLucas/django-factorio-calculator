from django.http import HttpResponse
from django.conf import settings
from django.urls import reverse
from jinja2.loaders import FileSystemLoader, PackageLoader, ChoiceLoader
from jinja2 import Environment

from os.path import join

__all__ = ('render_to_string', 'render_to_response')

loader_array = []
for pth in getattr(settings, 'TEMPLATE_DIRS', ()):
    loader_array.append(FileSystemLoader(join(settings.BASE_DIR, pth)))

for app in settings.INSTALLED_APPS:
    loader_array.append(PackageLoader(app))

default_mimetype = getattr(settings, 'DEFAULT_CONTENT_TYPE')
global_exts = getattr(settings, 'JINJA_EXTS', ())


class LazyEnv(object):
    def __init__(self):
        self._environment = None

    def __getattr__(self, name):
        if self._environment is None:
            self._environment = Environment(
                extensions=global_exts, loader=ChoiceLoader(loader_array))
            self._environment.globals['STATIC_URL'] = settings.STATIC_URL
            self._environment.globals['url'] = reverse
        if name == '__members__':
            return dir(self._environment)
        return getattr(self._environment, name)


env = LazyEnv()


def render_to_string(filename, context={}):
    template = env.get_template(filename)
    rendered = template.render(**context)
    return rendered


def render_to_response(filename, context={}, request=None,
                       mimetype=default_mimetype):
    if request:
        context['request'] = request
        context['user'] = request.user
    rendered = render_to_string(filename, context)
    return HttpResponse(rendered, content_type=mimetype)
