import lupa
import os

from django.conf import settings

MINING_TIME_MAPPING = {
    'uranium-ore': 4,
}


def dictify_lua_table(table):
    k_vs = list(table.items())
    if all(type(k) is int for k, v in k_vs):
        is_list = True
        result = []
    else:
        is_list = False
        result = {}

    for key, item in k_vs:
        if 'lupa' in str(type(item)):
            item = dictify_lua_table(item)
        if is_list:
            result.append(item)
        else:
            result[key] = item
    return result


def recursive_default_merge(a, b):
    for key in b.keys():
        if type(b[key]) is not dict:
            a.setdefault(key, b[key])
        else:
            a.setdefault(key, type(b)())
            recursive_default_merge(a[key], b[key])


def read_factorio_recipes():
    lua = lupa.LuaRuntime()
    data_path = os.path.join(settings.FACTORIO_BASE_PATH, 'data', 'base',
                             'prototypes')

    with open(os.path.join(settings.FACTORIO_BASE_PATH,
              'data', 'core', 'lualib', 'dataloader.lua')) as fp:
        lua.execute(fp.read())

    return_func = '''
    function()
        return data
    end
    '''

    for sub_path in ['item', 'recipe', 'fluid']:
        for item in os.listdir(os.path.join(data_path, sub_path)):
            with open(os.path.join(data_path, sub_path, item)) as fp:
                content = fp.read()
                content = '\n'.join(
                    x for x in content.splitlines()
                    if 'sound =' not in x and 'require (' not in x and
                    'require(' not in x)
                lua.execute(content)

    getdata = lua.eval(return_func)
    _data = getdata()
    base = dictify_lua_table(_data.raw)

    recipes = base['recipe']

    for key in [key for key in base.keys() if key is not 'recipe']:
        recursive_default_merge(recipes, base[key])

    delete_them = []

    # Factorio has some (as of now, to me) weird way of describing it's
    # prototypes across all .lua files.
    for key, recipe in recipes.items():
        has_ingr = 'ingredients' in recipe.keys()
        raw_or_int = recipe.get('subgroup', None) in {
            'raw-resource', 'intermediate-product', 'raw-material'}
        is_hidden = 'hidden' in recipe.get('flags', [])

        fluid_or_recipe = recipe.get('type', None) in {'fluid', 'recipe'}
        if ((not has_ingr and not raw_or_int and not fluid_or_recipe) or
                is_hidden):
            delete_them.append(key)

        if (
                recipe.get('energy_required', recipe.get('normal', {})
                           .get('energy_required', None))) is None:
            if recipe.get('subgroup', None) in {'raw-resource',
                                                'raw-material'}:
                recipe['energy_required'] = MINING_TIME_MAPPING.get(key, 2)
            else:
                recipe['energy_required'] = 0.5

    for key in delete_them:
        del recipes[key]

    return recipes
