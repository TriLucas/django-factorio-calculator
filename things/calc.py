from django.db.models import Max

from things.models import Product

from libs.jinja import render_to_string


class Calculator(object):
    def __init__(self, target_product, output_per_second=None, machines=None,
                 max_depth=1):
        self._target_product = target_product

        self.max_depth = max_depth

        self._subs = []

        if output_per_second is not None and machines is not None:
            raise ValueError(('You may only specify one of output_per_second '
                              'or machines.'))

        pp_qset = target_product.pprocess.filter(is_input=False).exclude(
            process__pk__in=target_product.pprocess.filter(
                is_input=True).values('process__pk'))

        max_speed = pp_qset.aggregate(
            speed=Max('process__doable_in__speed'))['speed']

        self.process_product = pp_qset[0]
        self.process = self.process_product.process

        speed_factor = self.process.production_time / max_speed
        if output_per_second is not None:
            self._machines = (
                (speed_factor / self.process_product.amount) /
                (1 / output_per_second))
            self._output_per_second = output_per_second
        else:
            self._machines = machines
            self._output_per_second = machines / speed_factor

    def calculate(self):
        result = {
            self._target_product: {
                'machines': self._machines,
                'product_per_second': self._output_per_second,
            }
        }

        for ingredient in self.process.pproduct.filter(is_input=True):
            target_output = self._output_per_second * ingredient.amount

            int_calculator = Calculator(ingredient.product,
                                        output_per_second=target_output,
                                        max_depth=self.max_depth)

            self._subs.append(int_calculator)

            all_machines = int_calculator.calculate()
            for key, stats in all_machines.items():
                result.setdefault(key, {})
                result[key]['machines'] = (
                    stats['machines'] +
                    result[key].get('machines', 0))

                result[key]['product_per_second'] = (
                    stats['product_per_second'] +
                    result[key].get('machines', 0))

        return result

    def __str__(self):
        context = {}
        context['product'] = self._target_product
        context['machines'] = {
            Product.objects.get(id=k): v for k, v in self.calculate().items()}
        context['root_machines'] = self._machines
        context['output_per_second'] = self._output_per_second
        return render_to_string('product.html', context=context)

    def render_this(self, depth=0):
        context = {
            'depth': depth,
            'product': self._target_product,
            'output_per_second': self._output_per_second,
            'machines': self._machines,
            'extra': (''.join(x.render_this(depth + 1) for x in self._subs)
                      if depth < self.max_depth else '')
        }
        return render_to_string('product.html', context)

    def render(self):
        result = self.calculate()

        context = {}
        context['machines'] = result
        context['extra'] = self.render_this()
        return render_to_string('sum.html', context=context)

    def __str__(self):
        return self.render()
