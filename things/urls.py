from django.urls import path

from things import views

urlpatterns = [
    path('', views.calculate, name='calculate'),
    path('<slug:product_name>/', views.calculate),
]
