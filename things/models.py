from django.db import models
from django.utils.html import format_html


class Machine(models.Model):
    name = models.CharField(blank=False, max_length=250)
    speed = models.FloatField(blank=False, default=1.0)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(blank=False, max_length=250)
    image = models.CharField(blank=False, max_length=250)

    def admin_img(self):
        return format_html('<img src="{}"/>'.format(self.image))
    admin_img.short_description = 'Image'
    admin_img.allow_tags = True

    def __str__(self):
        return self.name


class Process(models.Model):
    name = models.CharField(blank=False, max_length=250)
    production_time = models.FloatField(blank=False)
    products = models.ManyToManyField(Product, related_name='process',
                                      through='things.ProcessProducts')
    doable_in = models.ManyToManyField(Machine)
    image = models.CharField(blank=True, default=None, null=True,
                             max_length=250)

    def __str__(self):
        return self.name

    def admin_img(self):
        if self.image is not None:
            return format_html('<img src="{}"/>'.format(self.image))
        return 'No Image'
    admin_img.short_description = 'Image'
    admin_img.allow_tags = True


class ProcessProducts(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                related_name='pprocess')
    process = models.ForeignKey(Process, on_delete=models.CASCADE,
                                related_name='pproduct')
    amount = models.IntegerField(blank=False)
    is_input = models.BooleanField(blank=False, default=True)

    def __str__(self):
        return 'Products involved in ' + str(self.process)
