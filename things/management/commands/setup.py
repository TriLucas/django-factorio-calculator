import os

from django.core.management.base import BaseCommand
from django.db import transaction

from things.models import Product
from things.models import Machine
from things.models import Process
from things.models import ProcessProducts

from libs.factoriolua import read_factorio_recipes
from things.utils import get_display_name

MINING_TIME_MAPPING = {
    'uranium-ore': 4,
}

OUTPUT_MAPPING = {
    'raw-wood': 4,
}


class Command(BaseCommand):
    help = 'Sync database with products in static/icons/'

    def is_centrifuging(self, recipe):
        return recipe.get('category', None) == 'centrifuging'

    def is_fluid_recipe(self, recipe):
        return recipe.get('subgroup', None) == 'fluid-recipes'

    def is_fluid(self, recipe):
        return recipe.get('default_temperature', None) is not None

    def get_image_path(self, recipe):
        try:
            return os.path.join('/', 'static',
                                recipe['icon'].replace('__base__/', ''))
        except KeyError:
            return ''

    def product_and_amount(self, ingredient):
        if type(ingredient) is dict:
            product = Product.objects.get(
                name=get_display_name(ingredient['name']))
            amount = ingredient['amount']
        else:
            product = Product.objects.get(
                name=get_display_name(ingredient[0]))
            try:
                amount = ingredient[1]
            except IndexError:
                amount = 1

        return product, amount

    @transaction.atomic
    def handle(self, *args, **kwargs):
        for name, speed in [
                ('Burner Mining Drill', 0.35),
                ('Electric Mining Drill', 0.5),
                ('Assembling Machine 1', 0.5),
                ('Assembling Machine 2', 0.75),
                ('Assembling Machine 3', 1.25),
                ('Stone Furnace', 1),
                ('Steel Furnace', 2),
                ('Electric Furnace', 2),
                ('Oil Refinery', 1),
                ('Chemical Plant', 1.25),
                ('Centrifuge', 0.75),
                ('Offshore Pump', 1200),
                ('Pumpjack', 100)]:
            Machine.objects.get_or_create(name=name, speed=speed)

        recipes = read_factorio_recipes()

        processes = {
            k: v for k, v in [(k, v) for k, v in recipes.items() if (
                self.is_fluid_recipe(v) and not
                self.is_fluid(v) or self.is_centrifuging(v))]}

        products = {
            k: v for k, v in [(k, v) for k, v in recipes.items() if ((
                not self.is_fluid_recipe(v) or self.is_fluid(v)) and (not
                    self.is_centrifuging(v) or v['name'] == 'nuclear-fuel'))]}

        drilling = Process.objects.create(name='Drilling', image=None,
                                          production_time=1)
        drilling.doable_in.add(Machine.objects.get(name='Pumpjack'))

        pumping = Process.objects.create(name='Pumping', image=None,
                                         production_time=1)
        pumping.doable_in.add(Machine.objects.get(name='Offshore Pump'))

        # create basic processes
        for name, recipe in processes.items():
            true_name = get_display_name(name)
            image_path = self.get_image_path(recipe)

            Process.objects.create(
                name=true_name,
                image=image_path,
                production_time=recipe.get('energy_required', 0.5),
            )

        # create basic products
        for name, recipe in products.items():
            true_name = get_display_name(name)
            image_path = self.get_image_path(recipe)

            Product.objects.create(
                name=true_name,
                image=image_path)

        for name, recipe in processes.items():
            the_process = Process.objects.get(name=get_display_name(name))
            for ingredient in recipe['ingredients']:
                product, amount = self.product_and_amount(ingredient)
                ProcessProducts.objects.create(
                    process=the_process,
                    product=product,
                    amount=amount,
                    is_input=True)
            if 'results' in recipe:
                for ingredient in recipe['results']:
                    product, amount = self.product_and_amount(ingredient)
                    ProcessProducts.objects.create(
                        process=the_process,
                        product=product,
                        amount=amount,
                        is_input=False)
            elif 'result' in recipe:
                product, amount = self.product_and_amount([recipe['result'], ])
                ProcessProducts.objects.create(
                    process=the_process,
                    product=product,
                    amount=amount,
                    is_input=False)

            if recipe['category'] == 'chemistry':
                the_process.doable_in.add(Machine.objects.get(
                    name='Chemical Plant'))
            elif self.is_centrifuging(recipe):
                the_process.doable_in.add(Machine.objects.get(
                    name='Centrifuge'))
            elif recipe['category'] == 'oil-processing':
                the_process.doable_in.add(Machine.objects.get(
                    name='Oil Refinery'))

        for name, recipe in products.items():
            the_product = Product.objects.get(name=get_display_name(name))

            result_count = 1
            if 'result_count' in recipe:
                result_count = recipe.get('result_count',
                                          OUTPUT_MAPPING.get(name, 1))
            elif 'results' in recipe:
                results = [x for x in recipe.get('results', [])
                           if x['name'] == name][0]
                result_count = results.get('amount', 1)

            if not ProcessProducts.objects.filter(product__pk=the_product.pk,
                                                  is_input=False):
                raw = name in {'water', 'crude-oil', 'stone', 'coal'}
                if raw or 'ore' in name:
                    if name == 'water':
                        process = Process.objects.get(name='Pumping')
                    elif name == 'crude-oil':
                        process = Process.objects.get(name='Drilling')
                    else:
                        process = Process.objects.create(
                            name='Mining ' + the_product.name,
                            production_time=recipe.get(
                                'energy_required',
                                MINING_TIME_MAPPING.get(name, 2)))
                        process.doable_in.add(
                            Machine.objects.get(name='Burner Mining Drill'))
                        process.doable_in.add(
                            Machine.objects.get(name='Electric Mining Drill'))

                    ProcessProducts.objects.create(process=process,
                                                   product=the_product,
                                                   amount=result_count,
                                                   is_input=False)
                else:
                    process = Process.objects.create(
                        name='Crafting ' + the_product.name,
                        production_time=recipe.get(
                            'energy_required',
                            recipe.get('normal', {}).get(
                                'energy_required', 0.5)))
                    if 'plate' in name:
                        process.doable_in.add(Machine.objects.get(
                            name='Stone Furnace'))
                        process.doable_in.add(Machine.objects.get(
                            name='Steel Furnace'))
                        process.doable_in.add(Machine.objects.get(
                            name='Electric Furnace'))
                    elif recipe['type'] != 'chemistry':
                        process.doable_in.add(Machine.objects.get(
                            name='Assembling Machine 1'))
                        process.doable_in.add(Machine.objects.get(
                            name='Assembling Machine 2'))
                        process.doable_in.add(Machine.objects.get(
                            name='Assembling Machine 3'))
                    else:
                        process.doable_in.add(Machine.objects.get(
                            name='Chemical Plant'))

                    ProcessProducts.objects.create(
                        product=the_product,
                        process=process,
                        amount=result_count,
                        is_input=False)

                    ingredients = recipe.get(
                        'ingredients', recipe.get('normal', {}).get(
                            'ingredients', None))
                    if ingredients:
                        for ingredient in ingredients:
                            product, amount = self.product_and_amount(
                                ingredient)
                            ProcessProducts.objects.create(
                                process=process,
                                product=product,
                                amount=amount,
                                is_input=True)
