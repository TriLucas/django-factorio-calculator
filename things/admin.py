from django.contrib import admin
from django.forms import inlineformset_factory
from django import forms

from things.models import Product
from things.models import Machine
from things.models import Process
from things.models import ProcessProducts


class CustomProcessProductFormset(forms.BaseInlineFormSet):
    def __init__(self, *args, instance=None, queryset=None, **kwargs):
        queryset = queryset.filter(is_input=self.is_input)
        instance = super(CustomProcessProductFormset, self).__init__(
            *args, instance=instance, queryset=queryset, **kwargs)


ProcessProductsFormset = inlineformset_factory(
    Product, ProcessProducts, fields=('product', 'amount'),
    formset=CustomProcessProductFormset)


class ProcessProductsInput(admin.TabularInline):
    model = ProcessProducts
    extra = 0
    fk_name = 'product'
    formset = ProcessProductsFormset

    verbose_name_plural = 'Is input for'


class ProcessProductsOutput(admin.TabularInline):
    model = ProcessProducts
    extra = 0
    fk_name = 'product'
    formset = ProcessProductsFormset

    verbose_name_plural = 'Made of'


class ProductAdmin(admin.ModelAdmin):
    inlines = [
        ProcessProductsOutput,
        ProcessProductsInput,
    ]

    def get_formsets_with_inlines(self, request, obj=None):
        for inline in self.get_inline_instances(request, obj):
            formset = inline.get_formset(request, obj)
            formset.is_input = isinstance(inline, ProcessProductsInput)
            yield formset, inline

    list_display = ('admin_img', 'name')


class ProcessProductsAdmin(admin.TabularInline):
    model = ProcessProducts
    extra = 2
    fk_name = 'process'


class ProcessAdmin(admin.ModelAdmin):
    inlines = [
        ProcessProductsAdmin,
    ]

    list_display = ('admin_img', 'name')

    def get_queryset(self, request):
        qs = super(ProcessAdmin, self).get_queryset(request)
        return qs.filter(image__isnull=False)


admin.site.register(Machine)
admin.site.register(Product, ProductAdmin)
admin.site.register(Process, ProcessAdmin)

# Register your models here.
