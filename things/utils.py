def get_display_name(name):
    return ' '.join(name.split('-')).capitalize()
