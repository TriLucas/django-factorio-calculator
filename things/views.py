from things.models import Product
from things.calc import Calculator
from things.utils import get_display_name

from django.shortcuts import render

from django import forms

CHOICES = [('Machines', 'Machines'),
           ('Output', 'Output')]


class ImageChoiceWidget(forms.widgets.Select):
    template_name = 'things/image_select.html'
    option_template_name = 'things/image_select_option.html'
    class Media:
        css = {'all': ('dropdown.css',)}
        js = ('dropdown.js',)

    def format_value(self, value):
        return value

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value or [], attrs)
        if value:
            product = Product.objects.get(id=value)
            context['widget']['label'] = product.name
            context['widget']['image'] = product.image
        return context

    def create_option(self, name, value, label, selected, initial, subindex=None, attrs=None):
        context = super().create_option(name, value, label, self, initial, subindex, attrs)
        if value:
            context['image'] = Product.objects.get(id=value).image
        return context


class CalculateForm(forms.Form):
    product = forms.ModelChoiceField(
        queryset=Product.objects.order_by('name'),
        widget=ImageChoiceWidget,
        initial=Product.objects.get(id=1))
    number = forms.FloatField(initial=3)
    switch = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect(), initial='Output')
    max_depth = forms.IntegerField(initial=3)


def calculate(request, product_name=None):
    if product_name is not None:
        prod = Product.objects.get(name=get_display_name(product_name))
    else:
        prod = None

    calculator = 'Please choose a Product.'
    if request.method == 'POST':
        data = request.POST
    else:
        data = None

    theform = CalculateForm(data, initial={'product': prod, 'max_depth': 1})

    if theform.is_valid():
        machines = None
        output_per_second = None

        if theform.cleaned_data['switch'] == 'Machines':
            machines = theform.cleaned_data['number']
        else:
            output_per_second = theform.cleaned_data['number']

        calculator = Calculator(theform.cleaned_data['product'],
                                output_per_second,
                                machines, theform.cleaned_data['max_depth'])

    return render(request, 'base.html',
                  context={'form': theform,
                           'content': str(calculator)})
